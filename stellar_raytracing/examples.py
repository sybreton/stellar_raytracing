from stellar_raytracing import perform_raytracing_2d 
import numpy as np
import importlib.resources
import stellar_raytracing.profiles as profiles

'''
Provide a set of examples to test the code behaviour.
'''

def get_profile (case='solar', wave_type='gravity') :
  '''
  Get Brunt-Vaisala or sound speed profile for a given
  case.
  '''
  if 'gravity' in wave_type :
    if case=='solar' :
      file_to_get = 'solar_bv_profile.dat' 
    elif case=='fstar' :
      file_to_get = 'fstar_bv_profile.dat'
  if 'acoustic' in wave_type :
    if case=='solar' :
      raise Exception ("acoustic case is not implemented yet")
    elif case=='fstar' :
      file_to_get = 'fstar_soundspeed_profile.dat'

  fp = importlib.resources.path (profiles, file_to_get)

  with fp as filename :
      data = np.loadtxt (filename)
  rs = data[:,0]
  if 'gravity' in wave_type :
    profile = 2*np.pi*data[:,1]*1e-6
  else : 
    profile = data[:,1]
 
  return rs, profile

def solar_case (tmax=86400, l=1, nu0=100e-6, r0=0.3,
                wave_type='gravity_solid_rotation', f=0,
                show=True, max_step=5) :
  '''
  Perform raytracing for a 1 Msun star with solar metallicity
  (profile computed with MESA).
  '''

  if wave_type=='acoustic' :
    raise Exception ("acoustic case is not implemented yet")

  if 'gravity' in wave_type :
    fp = importlib.resources.path (profiles, 'solar_bv_profile.dat')
    with fp as filename :
      data = np.loadtxt (filename)
    rs = data[:,0]
    bv = 2*np.pi*data[:,1]*1e-6

  omega0 = nu0 * 2*np.pi
  rstar = 6.9634e8
  r0 = r0 * rstar
  r_tachocline = rs[np.where ((bv==0)&(rs>0.65*rstar))[0][0]]
  radii_to_show = [r_tachocline]

  if f > 0 :
    Ro = omega0 / f
    print ('Wave Rossby number: {:.2f}'.format (Ro))

  t, y, omega = perform_raytracing_2d  (rs, bv, omega0, r0, np.pi/2, l,
                                        tmax, method='RK45', wave_type=wave_type, f=f,
                                        show=show, radii_to_show=radii_to_show,
                                        freq_tol_init=0.1, freq_tol_max=0.5,
                                        release_frequency_constraint=True, max_step=max_step,
                                        freq_tol_step=0.02)

  return t, y, omega 


def fstar (tmax=86400, l=1, nu0=300e-6, r0=0.2, 
           wave_type='gravity_solid_rotation', f=0, 
           show=True, max_step=1) :
  '''
  Perform raytracing for a 1.3 Msun F star.
  '''

  if wave_type=='acoustic' :
    fp = importlib.resources.path (profiles, 'fstar_soundspeed_profile.dat')
    with fp as filename :
      data = np.loadtxt (filename)
    rs = data[:,0]
    profile = data[:,1]

  if 'gravity' in wave_type :
    fp = importlib.resources.path (profiles, 'fstar_bvsquare_profile.dat')
    with fp as filename :
      data = np.loadtxt (filename)
    rs = data[:,0]
    bvsquare = data[:,1]
    bvsquare[bvsquare < 0] = 0
    profile = np.sqrt (bvsquare)

  omega0 = nu0 * 2*np.pi
  rstar = 1.0465e9
  r0 = r0 * rstar
  radii_to_show = [0.06*rstar, 0.88*rstar]

  if f > 0 :
    Ro = omega0 / f
    print ('Wave Rossby number: {:.2f}'.format (Ro))

  t, y, omega = perform_raytracing_2d  (rs, profile, omega0, r0, np.pi/2, l,
                                        tmax, method='RK45', wave_type=wave_type, f=f,
                                        show=show, radii_to_show=radii_to_show, 
                                        freq_tol_init=0.1, freq_tol_max=0.5,
                                        release_frequency_constraint=True, max_step=max_step, 
                                        freq_tol_step=0.02)

  return t, y, omega

if __name__=='__main__' :

  solar_case ()
  fstar ()
