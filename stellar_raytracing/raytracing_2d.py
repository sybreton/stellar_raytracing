import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp
from scipy.optimize import newton
import warnings

'''
Functions to perform two-dimension raytracing 
in stellar interiors.
'''

def norm_k (y) :
    '''
    Take the Hamiltonian coordinates and return wave vector norm.
    '''
    return np.sqrt (y[2]**2 + y[3]**2)

def interpolate_profile (r, rs, profile) :
    '''
    Interpolate profile at radius r.
    '''
    return np.interp (r, rs, profile)

def compute_frequency (rs, profile, y, f=0, wave_type='gravity') :
    '''
    Compute frequency given wave coordinates.
    '''

    k = norm_k (y) 
    if wave_type == 'gravity' :
      N = interpolate_profile (y[0], rs, profile)
      omega = np.sqrt (N**2 * y[3]**2 / k**2) 
    if wave_type == 'gravity_solid_rotation' :
      N = interpolate_profile (y[0], rs, profile)
      k = norm_k (y)
      omega = np.sqrt (N**2*y[3]**2/k**2 + f**2/k**2 * (np.cos (y[1])**2*y[2]**2 + np.sin(y[1])**2*y[3]**2 - y[2]*y[3]*np.sin(2*y[1])))  
    if wave_type == 'acoustic' :
      cs = interpolate_profile (y[0], rs, profile)
      omega = cs * k
      
    return omega

def compute_kr0_newton (rs, profile, r0, theta0, ktheta0, omega0, f=0, 
                        wave_type='gravity') :
    '''
    Use Newton method to constrain kr0 from other initial 
    conditions. 
    '''
  
    if wave_type == 'gravity' :
      N = interpolate_profile (r0, rs, profile)
      kr0 = np.sqrt (N**2 * ktheta0**2 / omega0**2 - ktheta0**2)

    if wave_type == 'gravity_solid_rotation' :
      N = interpolate_profile (r0, rs, profile)
      func_kr = lambda kr : N**2*ktheta0**2/(kr**2 + ktheta0**2) + \
                      f**2/(kr**2 + ktheta0**2) * (np.cos (theta0)**2*kr**2 + \
                      np.sin(theta0)**2*ktheta0**2 - kr*ktheta0*np.sin(2*theta0)) - omega0**2 
      x0 = np.sqrt ((N**2 + f**2 - omega0**2)  / omega0**2) *  ktheta0
      kr0 = newton(func_kr, x0, maxiter=1000)

    if wave_type == 'acoustic' :
      cs = interpolate_profile (r0, rs, profile)
      kr0 = np.sqrt (omega0**2 / cs**2 - ktheta0**2)

    return kr0

def compute_ktheta0 (r0, l) :
    '''
    Compute ktheta0 value considering initial radius and degree l.
    '''

    ktheta0 = np.sqrt (l*(l+1)) / r0

    return ktheta0

def compute_group_velocity (y, omega, rs, profile, f=0, wave_type='gravity') :
    '''
    Compute group velocity along the ray trajectory.
    '''

    dprofiledr = np.gradient (profile, rs)

    if wave_type=='acoustic' :
      vgr, vgtheta, _, _, = fun_acoustic (y, rs, profile, dprofiledr)
    elif wave_type=='gravity' :
      vgr, vgtheta, _, _, = fun_gravity (y, rs, profile, dprofiledr)
    elif wave_type=='gravity_solid_rotation' :
      vgr, vgtheta, _, _, = fun_gravity_solid_rotation (y, rs, profile, dprofiledr, f)
    else :
      raise Exception ("This wave type is not implemented yet.")

    return vgr, vgtheta

def plot_group_velocity (y, omega, rs, profile, f=0, Rstar=None, wave_type='gravity',
                         show=True, filename=None, xlabel=r'$r$ (m)', 
                         ylabel=r'$v$ (m/s)', figsize=(8,4)) :
    '''
    Compute and plot group velocity.
    '''

    if Rstar is None :
      Rstar = 1
    
    vgr, vgtheta = compute_group_velocity (y, omega, rs, profile, f=f, 
                                           wave_type=wave_type) 

    fig, ax = plt.subplots (1, 1, figsize=figsize)

    ax.plot (y[0]/Rstar, vgr, color='cornflowerblue', label=r'$v_{g,r}$')
    ax.plot (y[0]/Rstar, vgtheta, color='gold', label=r'$v_{g,\theta}$')

    ax.set_xlabel (xlabel)
    ax.set_ylabel (ylabel)

    ax.legend ()

    if filename is not None :
      plt.savefig (filename)
    if show :
      plt.show ()

    return fig

def plot_raytracing_2d (y, rs, profile, radii_to_show=None,
                        color='cornflowerblue', show=False, filename=None) :
    '''
    Plot integrated trajectories from ``perform_raytracing_2d``.
    '''

    fig, ax = plt.subplots (1, 1, figsize=(6,6))
    y[1,:] += np.pi/2 #Setting theta=0 as the vertical
    ax.plot (y[0,:]*np.cos(y[1,:]), y[0,:]*np.sin(y[1,:]), color=color)
    # plot surface radius of the star
    circle = matplotlib.patches.Circle ((0, 0), rs[-1], edgecolor='black', fill=False)
    ax.add_artist (circle)
    # plot additionnal radius wanted by the user
    if radii_to_show is not None :
      for radius in radii_to_show :
          circle = matplotlib.patches.Circle ((0, 0), radius, linestyle='--', 
                                              edgecolor='grey', fill=False)
          ax.add_artist (circle)

    ax.set_xlim (-rs[-1], rs[-1])
    ax.set_ylim (-rs[-1], rs[-1])
    plt.axis ('off')

    if filename is not None :
        plt.savefig (filename)
    if show :
        plt.show ()
    plt.close ()

def plot_parameter (t, y, omega, omega0, show=False, filename=None) :
  '''
  Plot temporal evolution of parameters.
  '''
  fig, axs = plt.subplots (4, 1, figsize=(8,8))
  axs[0].plot (t, (omega-omega0)/omega0*100, color='black')
  axs[0].axhline (0, color='cornflowerblue', zorder=-10, ls='--')
  axs[0].axhline (-0.5, color='firebrick', ls='--')
  axs[0].axhline (0.5, color='firebrick', ls='--')
  axs[1].plot (t, y[2,:], color='black')
  axs[2].plot (t, y[3,:], color='black')
  axs[3].plot (t, y[0,:], color='black')

  axs[0].set_ylabel (r'$\frac{\omega - \omega_0}{\omega_0} \times 100$')
  axs[1].set_ylabel (r'$k_r$ (m$^{-1}$)')
  axs[2].set_ylabel (r'$k_\theta$ (m$^{-1}$)')
  axs[3].set_ylabel (r'$r$ (m)')
  axs[3].set_xlabel ('Time (s)')

  fig.tight_layout ()

  if filename is not None :
    plt.savefig (filename)
  if show :
    plt.show ()
  plt.close ()

def fun_acoustic (y, rs, profile, dprofiledr) :
    '''
    Compute right terms of the Hamiltonian system for acoustic waves.
    '''
    cs = interpolate_profile (y[0], rs, profile)
    dcsdr = interpolate_profile (y[0], rs, dprofiledr)
    k = norm_k (y)
    dWdkr = cs * y[2] / k
    dWdktheta = cs * y[3] / k
    dWdr = k*dcsdr 
    dWdtheta = 0
    vector = np.array ([dWdkr, dWdktheta, dWdr, dWdtheta])

    return vector

def fun_gravity (y, rs, profile, dprofiledr) :
    '''
    Compute right terms of the Hamiltonian system for gravity waves.
    '''
    N = interpolate_profile (y[0], rs, profile)
    dNdr = interpolate_profile (y[0], rs, dprofiledr)
    k = norm_k (y)
    dWdkr = - N * y[2]*y[3] / k**3
    dWdktheta = N * y[2]**2 / k**3
    dWdr =  y[3] / k * dNdr 
    dWdtheta = 0
    vector = np.array ([dWdkr, dWdktheta, dWdr, dWdtheta])

    return vector

def fun_gravity_solid_rotation (y, rs, profile, dprofiledr, f) :
    '''
    Compute right terms of the Hamiltonian system for gravity waves considering
    solid rotation of the star.
    '''
    N = interpolate_profile (y[0], rs, profile)
    dNdr = interpolate_profile (y[0], rs, dprofiledr)
    k = norm_k (y)
    omega = compute_frequency (rs, profile, y, f=f, wave_type='gravity_solid_rotation')
    dWdkr = 1/(omega*k**2) * (-omega**2*y[2] + f**2*(np.cos(y[1])**2*y[2] - np.sin(2*y[1])*y[3]/2))
    dWdktheta = 1/(omega*k**2) * ((N**2-omega**2)*y[3] + f**2*(np.sin(y[1])**2*y[3] - np.sin(2*y[1])*y[2]/2))
    dWdr = 1/(omega*k**2) * (N*y[3]**2*dNdr) 
    dWdtheta = 1/(omega*k**2) * f**2 * (np.sin(2*y[1])/2*(y[3]**2 - y[2]**2) - np.cos(2*y[1])*y[2]*y[3])
    vector = np.array ([dWdkr, dWdktheta, dWdr, dWdtheta])

    return vector

def fun_gravity_shellular_rotation (y, rs, profile, dprofiledr) :
    #TODO
    '''
    Compute right terms of the Hamiltonian system for gravity waves considering
    shellular differential rotation of the star.
    '''
    warnings.warn ('Shellular rotation not implemented yet, solving equations for solid rotation.')
    
    N = interpolate_profile (y[0], rs, profile)
    dNdr = interpolate_profile (y[0], rs, dprofiledr)
    k = norm_k (y)
    dWdkr = - N * y[2]*y[3] / k**3
    dWdktheta = N * y[2]**2 / k**3
    dWdr =  y[3] / k * dNdr 
    dWdtheta = 0
    vector = np.array ([dWdkr, dWdktheta, dWdr, dWdtheta])

    return vector

#-------------------------------------------------------------------------
# Functions that are called by solve_ivp
# and therefore have same signature
def event_caustic (t, y, rs, profile, dprofiledr, omega0, wave_type, freq_tol, former_freq_tol,
                   f) :
    '''
    Check if frequency is changing beyond freq_tol.
    '''
    omega = compute_frequency (rs, profile, y, f=f, wave_type=wave_type)
    domegadt = compute_domegadt (t, y, rs, profile, dprofiledr, omega0, wave_type, freq_tol,
                                 former_freq_tol)
    #value will initially be positive and cross zero 
    #when gap between omega and omega0 is greater than 
    #freq_tol %
    value = freq_tol - 100 * np.abs (omega + domegadt - omega0) / omega0

    return value 

def inverse_event_caustic (t, y, rs, profile, dprofiledr, omega0, wave_type, freq_tol, 
                           former_freq_tol, f) :
    '''
    Check if frequency is again below previous freq_tol value.
    '''
    omega = compute_frequency (rs, profile, y, f=f, wave_type=wave_type)
    value = 100 * np.abs (omega - omega0) / omega0 - former_freq_tol

    return value 

def event_surface (t, y, rs, profile, dprofiledr, omega0, wave_type, freq_tol, 
                   former_freq_tol, f) :
    '''
    Check if wave has reached the surface (for the acoustic case).
    '''

    value = rs[-1] - y[0]

    return value 

event_surface.terminal = True
event_caustic.terminal = True
inverse_event_caustic.terminal = True
inverse_event_caustic.direction = -1

def fun (t, y, rs, profile, dprofiledr, omega0, wave_type, freq_tol, 
         former_freq_tol, f) :
    '''
    Compute right terms of the Hamiltonian system.
    '''
    if wave_type == 'acoustic' :
      dW = fun_acoustic (y, rs, profile, dprofiledr)
    if wave_type == 'gravity' : 
      dW = fun_gravity (y, rs, profile, dprofiledr)
    if wave_type == 'gravity_solid_rotation' : 
      dW = fun_gravity_solid_rotation (y, rs, profile, dprofiledr, f)
    drdt = dW[0]
    dthetadt = dW[1] / y[0]
    dkrdt = - dW[2] + dthetadt * y[3]
    dkthetadt = - dW[3] / y[0] - drdt / y[0] * y[3]

    vector = np.array ([drdt, dthetadt, dkrdt, dkthetadt])

    return vector 

def compute_domegadt (t, y, rs, profile, dprofiledr, omega0, wave_type, freq_tol,
                      former_freq_tol, f) :
    '''
    Compute temporal derivative of the wave frequency.
    '''

    if wave_type == 'acoustic' :
      dW = fun_acoustic (y, rs, profile, dprofiledr)
    if wave_type == 'gravity' : 
      dW = fun_gravity (y, rs, profile, dprofiledr)
    if wave_type == 'gravity_solid_rotation' : 
      dW = fun_gravity_solid_rotation (y, rs, profile, dprofiledr, f)
    drdt = dW[0]
    dthetadt = dW[1] / y[0]
    dkrdt = - dW[2] + dthetadt * y[3]
    dkthetadt = - dW[3] / y[0] - drdt / y[0] * y[3]
    vector = np.array ([drdt, dthetadt, dkrdt, dkthetadt])
    domegadt = dW[2]*vector[0] + dW[3]*vector[1] + dW[0]*vector[2] + dW[1]*vector[3] 

    return domegadt

 

#-------------------------------------------------------------------------

def perform_raytracing_2d  (rs, profile, omega0, r0, theta0, l,
                            tmax, method='RK45', wave_type='gravity', f=0, 
                            show=False, filename_plot2d=None, filename_plotparameter=None, 
                            filename_plotvg=None,
                            freq_tol_init=0.1, freq_tol_max=0.5, 
                            freq_tol_step=0.05, radii_to_show=None, 
                            release_frequency_constraint=False, max_step=None) :
    '''
    Solve Hamiltonian system in two dimensions.

    Returns
    -------
      time ``t`` array, coordinates ``y`` array
      and frequency ``omega`` array.

      The ``y`` output is a 4D array with (in this order)
      radius ``r``, angle ``theta``, radial wave vector ``kr``
      and horizontal wave vector ``ktheta``.
    '''

    if 'gravity' in wave_type :
        if release_frequency_constraint :
          events = None
        else :
          events = [event_caustic, inverse_event_caustic]
    if wave_type == 'acoustic' :
        events = event_surface

    dprofiledr = np.gradient (profile, rs)
    ktheta0 = compute_ktheta0 (r0, l)
    kr0 = compute_kr0_newton (rs, profile, r0, theta0, ktheta0, omega0, f=f, wave_type=wave_type)
    y0 = np.array ([r0, theta0, kr0, ktheta0])

    t_span = (0, tmax)
    status = 1
    t = np.array ([0])
    y = y0.reshape (4,1)
    freq_tol = freq_tol_init
    former_freq_tol = 0
    if max_step is None :
      max_step = 0.01 * 2*np.pi / omega0
    max_step_init = max_step
    while status == 1 :
        sol = solve_ivp (fun, t_span, y0, method=method, t_eval=None, dense_output=False, 
                         events=events, vectorized=False, args=(rs, profile, dprofiledr, omega0, 
                                                                wave_type, freq_tol, former_freq_tol,
                                                                f),
                         max_step=max_step)
        t = np.concatenate ((t, sol.t[1:]))
        y = np.concatenate ((y, sol.y[:,1:]), axis=1)
        if sol.status == 1 :
            t_span = (sol.t[-1], tmax)
            y0 = sol.y[:,-1]
            if 'gravity' in wave_type :
                # integration stopped because the time step was too large
                # to conserve the wave frequency or because it is possible
                # to increase again the timestep.
                if sol.t_events[0].size > 0 :
                  max_step = max_step / 2
                  freq_tol = min (freq_tol_max, freq_tol + freq_tol_step)
                  former_freq_tol = freq_tol_init / 2
                  print ('Current time: {:.4f}'.format (t_span[0]), 
                         'New freq tol: {:2f}'.format (freq_tol), 
                         'New max timestep: {:f}'.format (max_step))
                else :
                  max_step = max_step_init
                  freq_tol = freq_tol_init
                  former_freq_tol = 0
                  print ('Current time: {:.4f}'.format (t_span[0]), 
                         'New freq tol: {:2f}'.format (freq_tol), 
                         'New max timestep: {:f}'.format (max_step))
            if wave_type == 'acoustic' :
                # integration stopped because the wave reached the surface
                # and the reflection step has to be artificially made by 
                # reversing the radial wave vector.
                y0[0] = rs[-1] - 1e-6
                y0[2] = - y0[2]
                print ('Manually reflecting wave')
        status = sol.status

    print (sol.message)
    omega = compute_frequency (rs, profile, y, f=f, wave_type=wave_type)

    if show or filename_plot2d is not None :
        plot_raytracing_2d (y, rs, profile, show=show, filename=filename_plot2d, 
                            radii_to_show=radii_to_show)
    if show or filename_plotparameter is not None :
        plot_parameter (t, y, omega, omega0, show=show, filename=filename_plotparameter)
    if show or filename_plotvg is not None :
        plot_group_velocity (y, omega, rs, profile, f=f, Rstar=rs[-1],
                             wave_type=wave_type,
                             show=show, filename=None, xlabel=r'$r/R_\star$',
                             ylabel=r'$v$ (m/s)', figsize=(8,4))


    return t, y, omega

if __name__ == '__main__' :

  data = np.loadtxt ('profiles/fstar_bvsquare_profile.dat')
  rs = data[:,0]
  bvsquare = data[:,1]
  bvsquare[bvsquare < 0] = 0
  bv = np.sqrt (bvsquare)
  rstar = 1.0465e9
  r0 = 0.2 * rstar
  radii_to_show = [0.06*rstar, 0.88*rstar]
  omega0 = 60e-6 * 2*np.pi
  tmax = .5 * 86400
  l  = 1

  max_step = 1e-5 * 2*np.pi / omega0
  print ('Max step', max_step)
  f = 5e-4

  if f > 0 :
    Ro = omega0 / f
    print ('Wave Rossby number: {:.2f}'.format (Ro))
 
  t, y, omega = perform_raytracing_2d  (rs, bv, omega0, r0, np.pi/2, l,
                                 tmax, method='RK45', wave_type='gravity_solid_rotation', f=f, 
                                 show=True, radii_to_show=radii_to_show, freq_tol_init=0.1, freq_tol_max=0.5, 
                                 release_frequency_constraint=True, max_step=max_step, freq_tol_step=0.02)
                                 


